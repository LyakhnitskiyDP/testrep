# VIDEO STORE #

Web Application built for Video Store management.
Architecture: microservices with REST style of communication.

High-level overview of the system:
<br />
![uml_1](UML.svg)
<br />
Web Application microservice is responsible for presentational aspect of the application,
while the Business Logic microservice exposes API for business domain transformations.


## How to run ###
This one gradle command will boot up both microservices in parallel
```gradle app:bootRun web:bootRun```

