package webApp.controllers;

import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.*;

@Controller
public class MainMenuController {

	@GetMapping("/")
	public String getMainMenuPage(
			@RequestParam(value = "active", required = false, defaultValue = "1") Integer active,
			Model model) {

		model.addAttribute("active", active);
		return "mainMenu";
	}

	@PostMapping("/submit")
	public String submit(@RequestParam("email") String email,
			     @RequestParam("text") String text) {


	     System.out.println("Got email: " + email);
	     System.out.println("Got text: " + text);

	     return "mainMenu";
        }
}
